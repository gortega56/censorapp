//
//  CAMainMenuViewController.m
//  CensorApp
//
//  Created by Gabriel Ortega on 4/27/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import <MobileCoreServices/MobileCoreServices.h>
#import "CAMainMenuViewController.h"
#import "CACreateViewController.h"

static NSString * const kCAVideoPickerSegueID = @"kCAVideoPickerSegueID";
static NSString * const kCAUnwindToMainMenuSegueID = @"kCAUnwindToMainMenuSegueID";
static NSString * const kCAMainMenuCreateSegueID = @"kCAMainMenuCreateSegueID";

@interface CAMainMenuViewController () 

@property (weak, nonatomic) IBOutlet UIButton *createButton;
@property (weak, nonatomic) IBOutlet UIButton *browseButton;
@property (weak, nonatomic) IBOutlet UIButton *myButton;

@property (strong , nonatomic) NSURL *selectedVideoURL;
@end

@implementation CAMainMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)touchUpInsideCreateButton:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select a media source" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Take a picture" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera]) {
            UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
            pickerController.delegate = self;
            pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            pickerController.mediaTypes = @[(NSString *) kUTTypeMovie];
            [self presentViewController:pickerController animated:YES completion:nil];
        }
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Select from gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]) {
            UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
            pickerController.delegate = self;
            pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            pickerController.mediaTypes = @[(NSString *) kUTTypeMovie];
            [self presentViewController:pickerController animated:YES completion:nil];
        }
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:alertController completion:nil];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kCAMainMenuCreateSegueID]) {
        CACreateViewController *createViewController = segue.destinationViewController;
        createViewController.videoURL = _selectedVideoURL.copy;
        _selectedVideoURL = nil;
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self dismissViewControllerAnimated:picker completion:^{
        NSString *mediaType = info[UIImagePickerControllerMediaType];
        if ([mediaType isEqualToString:@"public.movie"]) {
            self.selectedVideoURL = info[UIImagePickerControllerMediaURL];
            [self performSegueWithIdentifier:kCAMainMenuCreateSegueID sender:self];
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:picker completion:nil];
}

@end

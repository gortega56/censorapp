//
//  CAPlayerView.h
//  CensorApp
//
//  Created by Gabriel Ortega on 3/23/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface CAPlayerView : UIView

@property (nonatomic, strong) AVPlayer *player;

@end

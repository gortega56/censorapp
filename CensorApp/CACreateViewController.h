//
//  ViewController.h
//  CensorApp
//
//  Created by Gabriel Ortega on 3/16/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CAPlayerView.h"

@interface CACreateViewController : UIViewController

@property (nonatomic, strong) NSURL *videoURL;

@property (weak, nonatomic) IBOutlet CAPlayerView *playerView;

@end


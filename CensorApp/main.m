//
//  main.m
//  CensorApp
//
//  Created by Gabriel Ortega on 3/16/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

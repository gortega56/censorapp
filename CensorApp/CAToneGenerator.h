//
//  ToneGenerator.h
//  CensorApp
//
//  Created by Gabriel Ortega on 3/23/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import <AudioUnit/AudioUnit.h>

@class CAToneGenerator;
@protocol CAToneGeneratorDelegate <NSObject>

@required
- (void)toneGenerator:(CAToneGenerator *)toneGenerator didWriteAsset:(AVAsset *)asset;

@end

@interface CAToneGenerator : NSObject

@property (nonatomic) AudioComponentInstance toneUnit;
@property (nonatomic) ExtAudioFileRef audioFileRef;
@property (nonatomic) double frequency;
@property (nonatomic) double sampleRate;
@property (nonatomic) double theta;
@property (nonatomic, weak) id<CAToneGeneratorDelegate> delegate;

- (void)toggleTone;
- (void)discardToneAssets;
@end

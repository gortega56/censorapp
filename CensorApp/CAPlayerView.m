//
//  CAPlayerView.m
//  CensorApp
//
//  Created by Gabriel Ortega on 3/23/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import "CAPlayerView.h"

@implementation CAPlayerView

+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (AVPlayer*)player {
    return [(AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVPlayer *)player {
    [(AVPlayerLayer *)[self layer] setPlayer:player];
}

@end

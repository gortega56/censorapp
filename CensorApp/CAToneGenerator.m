//
//  ToneGenerator.m
//  CensorApp
//
//  Created by Gabriel Ortega on 3/23/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import "CAToneGenerator.h"

OSStatus RenderTone(
                    void *inRefCon,
                    AudioUnitRenderActionFlags 	*ioActionFlags,
                    const AudioTimeStamp 		*inTimeStamp,
                    UInt32 						inBusNumber,
                    UInt32 						inNumberFrames,
                    AudioBufferList 			*ioData)

{
    // Fixed amplitude is good enough for our purposes
    const double amplitude = 0.25;
    
    // Get the tone parameters out of the view controller
    CAToneGenerator *toneGenerator =
    (__bridge CAToneGenerator *)inRefCon;
    double theta = toneGenerator.theta;
    double theta_increment = 2.0 * M_PI * toneGenerator.frequency / toneGenerator.sampleRate;
    
    // This is a mono tone generator so we only need the first buffer
    const int channel = 0;
    Float32 *buffer = (Float32 *)ioData->mBuffers[channel].mData;
    
    // Generate the samples
    for (UInt32 frame = 0; frame < inNumberFrames; frame++)
    {
        buffer[frame] = sin(theta) * amplitude;
        
        theta += theta_increment;
        if (theta > 2.0 * M_PI)
        {
            theta -= 2.0 * M_PI;
        }
    }
    
    ExtAudioFileWriteAsync(toneGenerator.audioFileRef, inNumberFrames, ioData);
    
    // Store the theta back in the view controller
    toneGenerator.theta = theta;
    return noErr;
}

@interface CAToneGenerator ()

@property (nonatomic, strong) NSMutableArray *toneAssets;

@end

@implementation CAToneGenerator

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSError *sessionError = nil;
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
        [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioSessionDidChangeInterruptionType:) name:AVAudioSessionInterruptionNotification object:nil];
        
        _toneAssets = [NSMutableArray new];
        _sampleRate = 44100.f;
        _frequency = 440.f;
    }
    
    return self;
}

- (void)dealloc
{
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setActive:NO error:&sessionError];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)createToneUnit
{
    // Configure the search parameters to find the default playback output unit
    // (called the kAudioUnitSubType_RemoteIO on iOS but
    // kAudioUnitSubType_DefaultOutput on Mac OS X)
    AudioComponentDescription defaultOutputDescription;
    defaultOutputDescription.componentType = kAudioUnitType_Output;
    defaultOutputDescription.componentSubType = kAudioUnitSubType_RemoteIO;
    defaultOutputDescription.componentManufacturer = kAudioUnitManufacturer_Apple;
    defaultOutputDescription.componentFlags = 0;
    defaultOutputDescription.componentFlagsMask = 0;
    
    // Get the default playback output unit
    AudioComponent defaultOutput = AudioComponentFindNext(NULL, &defaultOutputDescription);
    NSAssert(defaultOutput, @"Can't find default output");
    
    // Create a new unit based on this that we'll use for output
    OSErr err = AudioComponentInstanceNew(defaultOutput, &_toneUnit);
    NSAssert1(_toneUnit, @"Error creating unit: %hd", err);
    
    // Set our tone rendering function on the unit
    AURenderCallbackStruct input;
    input.inputProc = RenderTone;
    input.inputProcRefCon = (__bridge void *)(self);
    err = AudioUnitSetProperty(_toneUnit,
                               kAudioUnitProperty_SetRenderCallback,
                               kAudioUnitScope_Input,
                               0,
                               &input,
                               sizeof(input));
    NSAssert1(err == noErr, @"Error setting callback: %hd", err);
    
    // Set the format to 32 bit, single channel, floating point, linear PCM
    const int four_bytes_per_float = 4;
    const int eight_bits_per_byte = 8;
    AudioStreamBasicDescription streamFormat;
    streamFormat.mSampleRate = _sampleRate;
    streamFormat.mFormatID = kAudioFormatLinearPCM;
    streamFormat.mFormatFlags =
    kAudioFormatFlagsNativeFloatPacked | kAudioFormatFlagIsNonInterleaved;
    streamFormat.mBytesPerPacket = four_bytes_per_float;
    streamFormat.mFramesPerPacket = 1;
    streamFormat.mBytesPerFrame = four_bytes_per_float;
    streamFormat.mChannelsPerFrame = 1;
    streamFormat.mBitsPerChannel = four_bytes_per_float * eight_bits_per_byte;
    err = AudioUnitSetProperty (_toneUnit,
                                kAudioUnitProperty_StreamFormat,
                                kAudioUnitScope_Input,
                                0,
                                &streamFormat,
                                sizeof(AudioStreamBasicDescription));
    NSAssert1(err == noErr, @"Error setting stream format: %hd", err);
    
    NSString *destinationFilePath = [self toneFilePath];
    
    CFURLRef destinationURL = CFURLCreateWithFileSystemPath(kCFAllocatorDefault, (__bridge CFStringRef)destinationFilePath, kCFURLPOSIXPathStyle, false);
    
    
    OSStatus status = ExtAudioFileCreateWithURL(destinationURL, kAudioFileCAFType, &streamFormat, NULL, kAudioFileFlags_EraseFile, &_audioFileRef);
    CFRelease(destinationURL);
    NSAssert1(status == noErr, @"Error setting up file: %d", (int)status);
    
    status = ExtAudioFileSetProperty(_audioFileRef, kExtAudioFileProperty_ClientDataFormat, sizeof(AudioStreamBasicDescription), &streamFormat);
    NSAssert1(status == noErr, @"Error setting extension for file: %d", (int)status);
    
    status  = ExtAudioFileWriteAsync(_audioFileRef, 0, NULL);
    NSAssert1(status == noErr, @"Error setting up write buffer: %d", (int)status);
}

- (void)audioSessionDidChangeInterruptionType:(NSNotification *)notification
{

}

- (void)toggleTone
{
    if (_toneUnit)
    {
        AudioOutputUnitStop(_toneUnit);
        AudioUnitUninitialize(_toneUnit);
        AudioComponentInstanceDispose(_toneUnit);
        _toneUnit = nil;
        
        OSStatus status = ExtAudioFileDispose(_audioFileRef);
        printf("ExtAudioFileDispose: %d\n", (int)status);
        [self addToneAsset];
    }
    else
    {
        NSError *sessionError = nil;
        [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
        
        [self createToneUnit];
        
        // Stop changing parameters on the unit
        OSErr err = AudioUnitInitialize(_toneUnit);
        NSAssert1(err == noErr, @"Error initializing unit: %hd", err);
        
        // Start playback
        err = AudioOutputUnitStart(_toneUnit);
        NSAssert1(err == noErr, @"Error starting unit: %hd", err);
    }
}

- (NSString *)toneFilePath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return [[NSString alloc] initWithFormat:@"%@/tone%lu.caf", documentsDirectory, (unsigned long)_toneAssets.count];
}

- (void)addToneAsset
{
    NSURL *toneURL = [NSURL fileURLWithPath:[self toneFilePath]];
    AVURLAsset *toneAsset = [AVURLAsset assetWithURL:toneURL];
    [_toneAssets addObject:toneAsset];
    [_delegate toneGenerator:self didWriteAsset:toneAsset];
}

- (void)discardToneAssets
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error = nil;
    for (AVURLAsset *toneAsset in _toneAssets) {
        [fileManager removeItemAtURL:toneAsset.URL error:&error];
        
        if (error) {
            NSLog(@"Error removing file: %@", error.localizedDescription);
        }
    }
    
    [_toneAssets removeAllObjects];
}

@end

//
//  AppDelegate.h
//  CensorApp
//
//  Created by Gabriel Ortega on 3/16/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


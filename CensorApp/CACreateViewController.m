//
//  ViewController.m
//  CensorApp
//
//  Created by Gabriel Ortega on 3/16/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import "CACreateViewController.h"
#import "CAToneGenerator.h"
#import <MessageUI/MessageUI.h>
//#import "GTLYouTube.h"

@interface CACreateViewController () <UINavigationControllerDelegate, MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate, CAToneGeneratorDelegate>

@property (nonatomic, strong) CAToneGenerator *toneGenerator;
@property (nonatomic, strong) NSArray *toneAssets;
@property (nonatomic) CMTime audioActionStartTime;
@property (nonatomic) CMTime audioActionEndTime;

@property (nonatomic, strong) AVPlayerItem *mOriginalItem;

@property (nonatomic, strong) AVMutableComposition *mComposition;
@property (nonatomic, strong) AVMutableCompositionTrack *mAudioCompositionTrack;
@property (nonatomic, strong) AVMutableCompositionTrack *mToneCompositionTrack;
@property (nonatomic, strong) AVMutableAudioMixInputParameters *mAudioMixInputParams;
@property (nonatomic, strong) AVMutableAudioMix *mAudioMix;
@property (nonatomic, strong) id timeObserver;

@property (nonatomic, getter=isReviewing) BOOL reviewing;
@property (nonatomic, getter=isPlaying) BOOL playing;

@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *exportButton;
@property (weak, nonatomic) IBOutlet UIButton *trashButton;
@property (weak, nonatomic) IBOutlet UIButton *toneButton;

- (IBAction)audioActionTouchDown:(UIButton *)sender;
- (IBAction)audioActionTouchUp:(UIButton *)sender;
- (IBAction)playButtonTouch:(UIButton *)sender;
- (IBAction)exportButtonTouch:(UIButton *)sender;
- (IBAction)trashButtonTouch:(UIButton *)sender;

@end

@implementation CACreateViewController

#pragma mark - UIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    _mOriginalItem = [AVPlayerItem playerItemWithURL:_videoURL];
    self.playerView.player = [AVPlayer playerWithPlayerItem:_mOriginalItem];
   
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidPlayToEndTime:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];

    __weak typeof(self) createViewController = self;
    CMTime interval = CMTimeMake(1, self.playerView.player.currentTime.timescale);
    _timeObserver = [self.playerView.player addPeriodicTimeObserverForInterval:interval queue:NULL usingBlock:^(CMTime time) {
        double currentTime = CMTimeGetSeconds(createViewController.playerView.player.currentItem.currentTime);
        double duration = CMTimeGetSeconds(createViewController.playerView.player.currentItem.duration);
        double progress = (currentTime / duration);
        [createViewController.progressView setProgress:progress animated:(progress > 0)];
    }];
    
    _toneGenerator = [[CAToneGenerator alloc] init];
    _toneGenerator.delegate = self;
    
    [self addObserver:self forKeyPath:@"reviewing" options:NSKeyValueObservingOptionInitial | NSKeyValueObservingOptionNew context:0];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self initializeCompositionWithURL:_videoURL];
}

#pragma mark - Key Value Observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"reviewing"]){
        _exportButton.hidden = !self.isReviewing;
        _trashButton.hidden = !self.isReviewing;
        _toneButton.hidden = self.isReviewing;
    }
}

#pragma mark - AVFoundation

- (void)initializeCompositionWithURL:(NSURL *)url
{
    _mComposition = [AVMutableComposition composition];
    AVURLAsset *videoAsset = [AVURLAsset assetWithURL:_videoURL];
    CMTimeRange videoTimeRange = CMTimeRangeMake(kCMTimeZero, videoAsset.duration);
    
    // Add Video Track of Video Asset
    AVMutableCompositionTrack *mVideoCompositionTrack = [_mComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    mVideoCompositionTrack.preferredTransform = [[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] preferredTransform];
    [mVideoCompositionTrack insertTimeRange:videoTimeRange ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0]  atTime:kCMTimeZero error:NULL];
    
    // Add Audio Track of Video Asset
    _mAudioCompositionTrack = [_mComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    [_mAudioCompositionTrack insertTimeRange:videoTimeRange ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:NULL];
    _mToneCompositionTrack = [_mComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    _mAudioMixInputParams = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:_mAudioCompositionTrack];
}

- (void)playerItemDidPlayToEndTime:(NSNotification *)notification
{
    self.playing = NO;
    if (!self.isReviewing) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Save?" message:@"You can save your current edits or retry." preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Review" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            _mAudioMix = [AVMutableAudioMix audioMix];
            _mAudioMix.inputParameters = @[_mAudioMixInputParams];
            AVPlayerItem *pm = [AVPlayerItem playerItemWithAsset:_mComposition.copy];
            pm.audioMix = _mAudioMix;
            
            [self.playerView.player replaceCurrentItemWithPlayerItem:pm];
            self.reviewing = YES;
        }]];
        
        [alertController addAction:[UIAlertAction actionWithTitle:@"Discard" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
            [self resetPlayerItem];
        }]];

        [self presentViewController:alertController animated:YES completion:nil];
    }
    else {
        _exportButton.hidden = NO;
        _trashButton.hidden = NO;
        _toneButton.hidden = NO;
        [self resetPlayerItem];
    }
    
    [UIView animateWithDuration:0.15 animations:^{
        _playButton.alpha = 0.75f;
    }];
}

- (void)resetPlayerItem
{
    [_playerView.player seekToTime:kCMTimeZero];
}

- (void)toneGenerator:(CAToneGenerator *)toneGenerator didWriteAsset:(AVAsset *)asset
{
    CMTimeRange timeRange = CMTimeRangeMake(kCMTimeZero, asset.duration);
    [_mToneCompositionTrack insertTimeRange:timeRange ofTrack:[[asset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:_audioActionStartTime error:NULL];
    // TO DO: Have to clean up audio composition properties after session.

    [_mAudioMixInputParams setVolumeRampFromStartVolume:1.0 toEndVolume:0.0 timeRange:CMTimeRangeMake(_audioActionStartTime, kCMTimeZero)];
   [_mAudioMixInputParams setVolumeRampFromStartVolume:0.0 toEndVolume:1.0 timeRange:CMTimeRangeMake(_audioActionEndTime, kCMTimeZero)];
}

- (void)AVSessionDidExportFileToURL:(NSURL *)URL
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Export Successful" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController addAction:[UIAlertAction actionWithTitle:@"SMS" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self presentMessageComposeViewControllerWithURL:URL];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Email" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self presentMailComposeViewControllerWithURL:URL];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:alertController completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - Interactions

- (IBAction)audioActionTouchDown:(UIButton *)sender
{
    if (!self.isPlaying || self.isReviewing) {
        return;
    }
    
    _audioActionStartTime = _playerView.player.currentTime;
    [_toneGenerator toggleTone];
    [_playerView.player setVolume:0.0f];
}

- (IBAction)audioActionTouchUp:(UIButton *)sender
{
    if (!self.isPlaying || self.isReviewing) {
        return;
    }
    
    _audioActionEndTime = _playerView.player.currentTime;
    [_toneGenerator toggleTone];
    [_playerView.player setVolume:1.0f];
}

- (IBAction)playButtonTouch:(UIButton *)sender
{
    [UIView animateWithDuration:0.15 animations:^{
        sender.alpha = 0.0f;
    }];
    
    _audioActionEndTime = kCMTimeZero;
    [self.playerView.player play];
    _playing = YES;
}

- (IBAction)exportButtonTouch:(UIBarButtonItem *)sender
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [[NSString alloc] initWithFormat:@"%@/CensorTest.mov", documentsDirectory];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:&error];
        if (error) {
            NSLog(@"Error removing existing item: %@", error.localizedDescription);
        }
    }
    
    NSURL *URL = [NSURL fileURLWithPath:filePath];
    AVAssetExportSession *exportSession = [AVAssetExportSession exportSessionWithAsset:_mComposition presetName:AVAssetExportPresetHighestQuality];
    exportSession.audioMix = _mAudioMix;
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    exportSession.outputURL = URL;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        if (exportSession.error) {
            NSLog(@"Error: %@", exportSession.error.localizedDescription);
        }
        else {
            [self AVSessionDidExportFileToURL:URL];
        }
    }];
}

- (IBAction)trashButtonTouch:(UIButton *)sender
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Are you sure?!" message:nil preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self.playerView.player replaceCurrentItemWithPlayerItem:_mOriginalItem];
        [self resetPlayerItem];
        self.reviewing = NO;
        
        [self dismissViewControllerAnimated:alertController completion:nil];
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self dismissViewControllerAnimated:alertController completion:nil];
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)presentMessageComposeViewControllerWithURL:(NSURL *)URL
{
    MFMessageComposeViewController *messageComposeViewController = [[MFMessageComposeViewController alloc] init];
    messageComposeViewController.delegate = self;
    BOOL success = [messageComposeViewController addAttachmentURL:URL withAlternateFilename:@"CensorVideo.mov"];
    if (success) {
        [self presentViewController:messageComposeViewController animated:YES completion:nil];
    }
    else {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Oops!" message:@"There was an error attaching the file" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            [self dismissViewControllerAnimated:alertController completion:nil];
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

- (void)presentMailComposeViewControllerWithURL:(NSURL *)URL
{
    MFMailComposeViewController *mailComposeViewController = [[MFMailComposeViewController alloc] init];
    mailComposeViewController.delegate = self;
    [mailComposeViewController addAttachmentData:[NSData dataWithContentsOfURL:URL] mimeType:@"video/quicktime" fileName:@"CensorVideo.mov"];
    [self presentViewController:mailComposeViewController animated:YES completion:nil];
}

#pragma mark - MFMessageComposeViewControllerDelegate

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    switch (result) {
        case MessageComposeResultSent:
            break;
        case MessageComposeResultFailed:
            break;
        case MessageComposeResultCancelled:
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:controller completion:nil];
}

#pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    if (error) {
        NSLog(@"Error sending mail: %@", error.localizedDescription);
    }
    
    [self dismissViewControllerAnimated:controller completion:nil];
}


@end

//
//  CALoginViewController.m
//  CensorApp
//
//  Created by Gabriel Ortega on 4/27/15.
//  Copyright (c) 2015 Gabriel Ortega. All rights reserved.
//

#import "CALoginViewController.h"

@interface CALoginViewController ()

@end

@implementation CALoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
